#!/bin/sh

# Shell script for initialize CA server

# Constant
HOME=.
PATH_CONF=../config
PATH_DEFAULT_SSL_CONF=/etc/ssl
PATH_CA=../ca
PATH_PRIVATE=$PATH_CA/private
PATH_CSRS=$PATH_CA/csrs
PATH_NEWCERTS=$PATH_CA/newcerts

FILE_SSL_CONF=$PATH_CONF/openssl.cnf
FILE_CA_INDEX=$PATH_CA/index.txt
FILE_CA_SERIAL=$PATH_CA/serial
FILE_CA_KEY=$PATH_PRIVATE/ca.key
FILE_CA_CSR=$PATH_CA/ca.csr
FILE_CA_CERT=$PATH_CA/ca.crt

##########################
# Prepare CA environment #
##########################
echo "Create dependency files & directories"

mkdir -p $PATH_CA
echo "  * $PATH_CA"

mkdir -p $PATH_PRIVATE
echo "  * $PATH_PRIVATE"

mkdir -p $PATH_CSRS
echo "  * $PATH_CSRS"

mkdir -p $PATH_NEWCERTS
echo "  * $PATH_NEWCERTS"

if [ -f $FILE_CA_INDEX ]
    then
    echo "  * $FILE_CA_INDEX"
else
    echo "  * $FILE_CA_INDEX"
    > $FILE_CA_INDEX
fi

if [ -f $FILE_CA_SERIAL ]
    then
    echo "  * $FILE_CA_SERIAL"
else
    echo "  * $FILE_CA_SERIAL"
    echo "00F106" > $FILE_CA_SERIAL
fi

########################
# CA key & Certificate #
########################
echo "Create CA key & certificate"

if [ -f $FILE_CA_KEY ]
    then
    echo "  * exist CA key"
else
    echo "  * create CA key"
    openssl genrsa -des3 -out $FILE_CA_KEY 2048
    # openssl ecparam -name sect571r1 -genkey -out $FILE_CA_KEY
    sleep 1
fi

if [ -f $FILE_CA_CERT ]
    then
    echo "  * exist CA certificate"
else
    if [ -f $FILE_CA_CSR ]
        then
        echo "  * exist CA certificate signing request"
        echo "  * create CA certificate (with self signed)"
        openssl x509 -req -days 365 -in $FILE_CA_CSR -signkey $FILE_CA_KEY -out $FILE_CA_CERT
        sleep 1
    else
        echo "  * create CA certificate signing request"
        openssl req -sha256 -new -key $FILE_CA_KEY -out $FILE_CA_CSR
        sleep 1

        echo "  * create CA certificate (with self signed)"
        openssl x509 -req -days 36500 -in $FILE_CA_CSR -signkey $FILE_CA_KEY -out $FILE_CA_CERT
        sleep 1
    fi
fi

echo "  * replace ssl configuration file"
sudo cp $FILE_SSL_CONF $PATH_DEFAULT_SSL_CONF
